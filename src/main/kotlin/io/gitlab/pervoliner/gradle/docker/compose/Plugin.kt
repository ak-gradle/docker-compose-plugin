package io.gitlab.pervoliner.gradle.docker.compose

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task


/**
 * User: akoiro
 * Date: 2019-04-07
 */

enum class TaskDef(val command: String, val params: List<String> = emptyList(), val ignoreExitValue: Boolean = false) {
    build("up", listOf("--no-start")),
    up("up"),
    rm("rm", listOf("-f"), ignoreExitValue = true),
    start("start"),
    stop("stop"),
    logs("logs"),
    restart("restart")
}

class Plugin : Plugin<Project> {
    companion object {
        const val TASK_VOLUME_DIR_MK = "volume.dir.mk"
        const val TASK_VOLUME_DIR_CLEAN = "volume.dir.clean"
        internal fun imageName(image: String) = image.split("/").last().split(':')[0]
    }

    override fun apply(target: Project) {
        val extension = target.extensions.create("dockerCompose",
                Extension::class.java, target)

        target.afterEvaluate {
            it.logger.debug("dataDir: ${extension.dataDir}")
            it.logger.debug("services: ${extension.services()}")
            it.logger.debug("images: ${extension.images()}")
            it.logger.debug("volumes: ${extension.volumes()}")
            it.logger.debug("dataDirs: ${extension.dataDirs()}")

            CreateNetwork(target, extension).init()
            initServiceRm(target, extension)

            DockerTasks(target, extension).init()

            initAllTasks(target, extension)
        }

        target.tasks.register("config.show") { t ->
            t.doFirst {
                target.logger.lifecycle(extension.toString())
            }
        }

        target.tasks.register(TASK_VOLUME_DIR_MK) { t ->
            t.doFirst {
                extension.dataDirs().forEach { d ->
                    t.project.logger.lifecycle("mkdir: $d")
                    t.project.mkdir(d)
                }
            }
        }

    }

    private fun initServiceRm(target: Project, extension: Extension) {
        extension.services().forEach { s ->
            target.tasks.register("$s.rm") { task ->
                task.description = "remove service $s"
                task.doFirst {
                    target.exec { exec ->
                        exec.commandLine = listOf("docker", "service", "rm", "${extension.stack}_$s")
                    }
                }
            }
        }
    }

    private fun orderedDependsOn(task: Task, names: List<String>) {
        val tasks = names.map { task.project.tasks.getByName(it) }
        tasks.zipWithNext().forEach { it.second.mustRunAfter(it.first) }
        task.dependsOn(tasks)
    }


    private fun initAllTasks(project: Project, extension: Extension) {
        project.tasks.register(TASK_VOLUME_DIR_CLEAN) {
            it.doFirst { t ->
                project.logger.lifecycle("!!!! ALL DATA WILL BE DELETE !!!!: (yes,no)")
                val delete = System.`in`.bufferedReader().readLine()
                if (delete == "yes")
                    t.project.delete(extension.dataDir)
            }
        }
    }
}
