package io.gitlab.pervoliner.gradle.docker.compose

import org.gradle.api.Project
import org.yaml.snakeyaml.Yaml

/**
 * User: akoiro
 * Date: 2019-04-07
 */
open class Extension(private val project: Project) {
    companion object {
        const val DATA_DIR_PROP = "data.dir"
        const val DOCKER_REPOSITORY = "docker.repository"
        const val DOCKER_HUB = "docker.hub"
        const val DOCKER_NETWORK = "docker.network"
    }

    var dockerCompose: String = "docker-compose"
    var file: String = "docker-compose.yml"
    var dataDir: String = if (project.hasProperty(DATA_DIR_PROP)) "${project.property(DATA_DIR_PROP)}" else "${project.rootDir}/.data"

    var repository: String = project.findProperty(DOCKER_REPOSITORY).toString()
    var hub: String = project.findProperty(DOCKER_HUB).toString()
    var url: String = arrayOf(hub, repository).joinToString("/")
    var stack: String = project.name
    var usePull: Boolean = true
    var tag: String = project.version.toString()
    var args: Map<String, String> = emptyMap()
    var exclude: List<String> = emptyList()
    var network: String = project.findProperty(DOCKER_NETWORK)?.toString() ?: project.rootProject.name

    var environments: Map<String, String> = emptyMap()

    fun environmentsMap(): Map<String, String> {
        val result = HashMap<String, String>(this.environments)
        if (!result.containsKey(Env.DATA_DIR))
            result[Env.DATA_DIR] = this.dataDir
        if (!result.containsKey(Env.VERSION))
            result[Env.VERSION] = this.tag
        if (!result.containsKey(Env.NETWORK))
            result[Env.NETWORK] = this.network
        if (!result.containsKey(Env.REPOSITORY))
            result[Env.REPOSITORY] = "${this.hub}/${this.repository}"

        this.environments.toList().forEach { result[it.first] = it.second }

        return result
    }

    fun config(): Map<Any, Any> = Yaml().load<Map<Any, Any>>(project.file(this.file).inputStream())

    fun dataDirs(): List<String> {
        val envs = environmentsMap()
        return volumesMap().values
                .map { (it as Map<*, *>)["driver_opts"] as Map<*, *> }
                .filter { it["type"] == "none" }
                .map { it["device"] as String }
                .map { it.replace("\${${Env.DATA_DIR}}", dataDir) }
                .map { s ->
                    var result = s
                    envs.forEach { (k, v) -> result = result.replace("\${${k}}", v) }
                    result
                }
    }

    fun volumes(): List<String> = volumesMap().keys.toList()

    private fun volumesMap(): Map<String, Any> {
        val volumes = config()["volumes"]
        return if (volumes != null) (volumes as Map<String, Any>) else emptyMap()
    }

    fun servicesMap(): Map<String, *> = config()["services"] as Map<String, *>

    fun services(): List<String> = (config()["services"] as Map<String, *>).keys.toList()

    fun images(): Set<String> = (config()["services"] as Map<*, *>).values
            .map { (it as Map<*, *>)["image"] as String }
            .filter { it.startsWith("\${DOCKER_REPOSITORY}") }.toSet()

    fun withImage(service: String): String? =
            (config()["services"] as Map<String, Map<String, String>>)
                    .filter { it.key == service }.values
                    .firstOrNull { it.getValue("image").startsWith("$repository/") }
                    ?.get("image")

    override fun toString(): String {
        return "dockerCompose: $dockerCompose\n" +
                "file: $file\n" +
                "dataDir: $dataDir\n" +
                "repository: $repository\n" +
                "hub: $hub\n" +
                "url: $url\n" +
                "dockerArgs: $args\n" +
                "environments: ${environmentsMap()}"
    }
}
