package io.gitlab.pervoliner.gradle.docker.compose

import org.gradle.api.Project

/**
 * User: akoiro
 * Date: 2/23/20
 */
class Deploy(val target: Project, val extension: Extension) {
    companion object {
        const val DEPLOY_WITH_EXCLUDE = "deploy.with.exclude"
        const val DEPLOY = "deploy"
    }

    fun init(): Unit {
        target.tasks.register(DEPLOY_WITH_EXCLUDE) { t ->
            val cmd = listOf("docker", "stack", "deploy", "--prune", "--compose-file", "${target.projectDir.path}/docker-compose.yml", extension.stack)
            t.description = cmd.joinToString(" ")
            t.dependsOn(Plugin.TASK_VOLUME_DIR_MK)
            t.dependsOn(CreateNetwork.NETWORK_CREATE)
            t.doFirst {
                t.project.exec { e ->
                    e.environment = extension.environmentsMap()
                    e.commandLine = cmd
                    t.logger.lifecycle("commandLine: ${e.commandLine}")
                    t.logger.lifecycle("environment: ${e.environment}")
                }
            }
        }
        target.tasks.register(DEPLOY) { deploy ->
            deploy.dependsOn(DEPLOY_WITH_EXCLUDE)
            if (extension.exclude.isNotEmpty()) {
                extension.exclude.map { "$it.rm" }.map { target.tasks.getByName(it) }.forEach {
                    it.mustRunAfter(DEPLOY_WITH_EXCLUDE)
                    deploy.dependsOn(it.path)
                }
            }
        }
    }
}
