package io.gitlab.pervoliner.gradle.docker.compose

/**
 * User: akoiro
 * Date: 2/23/20
 */
object Env {
    const val DATA_DIR = "DATA_DIR"
    const val VERSION = "VERSION"
    const val NETWORK = "DOCKER_NETWORK"
    const val REPOSITORY = "DOCKER_REPOSITORY"
}