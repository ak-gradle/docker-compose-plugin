package io.gitlab.pervoliner.gradle.docker.compose

import org.gradle.api.Project

/**
 * User: akoiro
 * Date: 2019-07-06
 */
class DockerTasks(val target: Project, val extension: Extension) {

    fun init() {
        cleanTask()

        volumeRmTask()

        Deploy(target, extension).init()

        stackRmTask()

        loginTask()

        extension.images().forEach { i ->
            val name = Plugin.imageName(i)
            val tag = extension.environments["${name}_tag".toUpperCase()] ?: extension.tag
            pullTask(name, tag)
            pushTask(name, tag)
            buildTask(name, tag)
        }
        target.tasks.register("build") { t ->
            t.dependsOn(extension.images().map { Plugin.imageName(it) }.map { "$it.build" })
        }
        target.tasks.register("push") { t ->
            t.dependsOn(extension.images().map { Plugin.imageName(it) }.map { "$it.push" })
        }
        target.tasks.register("pull") { t ->
            t.dependsOn(extension.images().map { Plugin.imageName(it) }.map { "$it.pull" })
        }
        target.tasks.register("list") { t ->
            t.doFirst {
                extension.images()
                        .map { Plugin.imageName(it) }
                        .map { "${extension.url}/$it:${extension.tag}" }
                        .forEach { t.logger.lifecycle(it) }
            }
        }
    }

    private fun buildTask(name: String, tag: String) {
        val dockerDir = "${target.projectDir}/$name"
        target.tasks.register("$name.build") { t ->
            val cmd = mutableListOf("docker", "build")
            extension.args.toList().map { listOf("--build-arg", "${it.first}=${it.second}") }
                    .forEach { cmd.addAll(it) }
            cmd.addAll(listOf("--tag", "${extension.url}/$name:$tag", dockerDir))

            t.doFirst {
                t.project.exec { e ->
                    e.environment = extension.environmentsMap()
                    e.commandLine = cmd.toList()
                    t.logger.lifecycle("commandLine: ${e.commandLine}")
                    t.logger.lifecycle("environment: ${e.environment}")
                }
            }
        }
    }

    private fun loginTask() {
        target.tasks.register("login") { t ->
            val cmd = listOf("docker", "login",
                    "-p", t.project.property("docker.password"),
                    "-u", t.project.property("docker.user"),
                    extension.url)
            t.description = cmd.joinToString(" ")
            t.doFirst {
                t.project.exec { e ->
                    e.commandLine(cmd)
                }
            }
        }
    }

    private fun pushTask(name: String, tag: String) {
        target.tasks.register("$name.push") { t ->
            val cmd = listOf("docker", "push", "${extension.url}/$name:$tag")
            t.description = cmd.joinToString(" ")
            t.doFirst {
                t.project.exec { e ->
                    e.commandLine(cmd)
                }
            }
        }
    }

    private fun pullTask(name: String, tag: String) {
        target.tasks.register("$name.pull") { t ->
            val cmd = listOf("docker", "pull", "${extension.url}/$name:$tag")
            t.description = cmd.joinToString(" ")
            t.doFirst {
                t.project.exec { e ->
                    e.environment = extension.environmentsMap()
                    e.commandLine = cmd
                    t.logger.lifecycle("commandLine: ${e.commandLine}")
                    t.logger.lifecycle("environment: ${e.environment}")
                }
            }
        }
    }


    private fun stackRmTask() {
        target.tasks.register("rm") { t ->
            val cmd = listOf("docker", "stack", "rm", extension.stack)
            t.description = cmd.joinToString(" ")
            t.doFirst {
                t.project.exec { e ->
                    e.commandLine = cmd
                }
            }
        }
    }

    private fun volumeRmTask() {
        target.tasks.register("volume.rm") { t ->
            t.doFirst {
                extension.volumes().forEach { v ->
                    t.project.exec { e ->
                        e.environment = extension.environmentsMap()
                        e.commandLine = listOf("docker", "volume", "rm", "${extension.stack}_$v")
                        e.isIgnoreExitValue = true
                        t.logger.lifecycle("commandLine: ${e.commandLine}")
                        t.logger.lifecycle("environment: ${e.environment}")
                    }
                }
            }
        }
    }

    private fun cleanTask() {
        target.tasks.register("clean") { t ->
            t.doFirst {
                t.project.exec { e ->
                    e.environment = extension.environmentsMap()
                    e.commandLine = listOf("docker", "system", "prune", "-f")
                    e.isIgnoreExitValue = true
                    t.logger.lifecycle("commandLine: ${e.commandLine}")
                    t.logger.lifecycle("environment: ${e.environment}")

                }
            }
        }
    }

}
