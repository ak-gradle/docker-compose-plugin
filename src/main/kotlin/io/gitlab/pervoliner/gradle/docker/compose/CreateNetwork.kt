package io.gitlab.pervoliner.gradle.docker.compose

import org.gradle.api.Project

/**
 * User: akoiro
 * Date: 2/23/20
 */
class CreateNetwork(private val target: Project, private val extension: Extension) {
    companion object {
        const val NETWORK_CREATE = "network.create"
    }

    fun init(): Unit {
        target.tasks.register(NETWORK_CREATE) { t ->
            t.doFirst {
                val result = target.exec {
                    it.commandLine = listOf("docker", "network", "inspect", extension.network)
                    it.isIgnoreExitValue = true
                }

                if (result.exitValue != 0) {
                    target.exec {
                        it.commandLine = listOf("docker", "network", "create",
                                "--scope", "swarm", "--driver", "overlay", extension.network)
                    }
                } else {
                    target.logger.lifecycle("network ${extension.network} exists.")
                }
            }
        }
    }
}